# Overview

This repostiory is used to add edge-tpu-trait trait to an ACM-enabled Anthos cluster

> :warning: Verify there is NO `<primary-namespace>` namespace on the cluster BEFORE applying this Cluster Trait Repo IF the Primary Root Repo is `structured`.

## How to use

Must apply the following code snippet. This code will create a new `RepoSync` object pointing to the Cluster Trait Repo, and create an `ExternalSecret` that allows the `RootSync` to reference a private repository.

> :note: This can be applied via ACM updates IF the Primary Root Repo is `unstructured`, otherwise a manual `kubectl apply -f <file.yaml>` is required to install this CTR.

```yaml
apiVersion: configsync.gke.io/v1beta1
kind: RootSync
metadata:
  name: edge-tpu-trait-trait-sync
  namespace: config-management-system
spec:
  git:
    sourceFormat: "unstructured"
    repo: "https://gitlab.com/gcp-solutions-public/retail-edge/available-cluster-traits/edge-tpu-trait-anthos.git"
    branch: "main"
    dir: "/config"
    auth: "token"
    secretRef:
      name: edge-tpu-trait-git-creds               # matches the ExternalSecret spec.target.name below

---

apiVersion: external-secrets.io/v1beta1
kind: ExternalSecret
metadata:
  name: edge-tpu-trait-git-creds-es
  namespace: config-management-system
spec:
  refreshInterval: 1m
  secretStoreRef:
    kind: ClusterSecretStore
    name: gcp-secret-store
  target:                                       # K8s secret definition
    name: edge-tpu-trait-git-creds                  ############# Matches the secretRef above
    creationPolicy: Owner
  data:
  - secretKey: username                         # K8s secret key name inside secret
    remoteRef:
      key: edge-tpu-trait-access-token-creds    #  GCP Secret Name
      property: username                        # field inside GCP Secret
  - secretKey: token                            # K8s secret key name inside secret
    remoteRef:
      key: edge-tpu-trait-access-token-creds    #  GCP Secret Name
      property: token                           # field inside GCP Secret

```

### See TPU DevicePlugin on the server
```
kubectl get nodes -o json | jq -r '["HOST    ","TPUs"], ["-----------","------"], (.items[] | [.metadata.name, .status.allocatable."ensor.cc/tpu"]) | @tsv'
```

### Create GCP Secret for git-creds

Create the GCP Secret Manager secret used by `ExternalSecret` to proxy for K8s `Secret`

```
export PROJECT_ID=<your google project id>
export SCM_TOKEN_TOKEN=<your gitlab personal-access token value>
export SCM TOKEN_USER=<your gitlab personal-access token user>

gcloud secrets create edge-tpu-trait-access-token-creds --replication-policy="automatic" --project="${PROJECT_ID}"
echo -n "{\"token\"{{':'}} \"${SCM_TOKEN_TOKEN}\", \"username\"{{':'}} \"${SCM_TOKEN_USER}\"}" | gcloud secrets versions add edge-tpu-trait-access-token-creds --project="${PROJECT_ID}" --data-file=-

```

## Local Validation

Assuming `nomos` is installed (via `gcloud components install nomos`)

```
nomos vet --no-api-server-check --path config/
```

### Docker method

Using this link to find the version of nomos-docker:  https://cloud.google.com/anthos-config-management/docs/how-to/updating-private-registry#expandable-1

```
docker pull gcr.io/config-management-release/nomos:stable
docker run -it -v $(pwd):/code/ gcr.io/config-management-release/nomos:stable nomos vet --no-api-server-check --path /code/config/
```

### ACM Overview

See [our documentation](https://cloud.google.com/anthos-config-management/docs/repo) for how to use each subdirectory.
